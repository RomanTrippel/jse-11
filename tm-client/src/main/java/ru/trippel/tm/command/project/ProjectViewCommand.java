package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.Project;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.util.ProjectPrintUtil;

import java.util.List;

@NoArgsConstructor
public final class ProjectViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project view";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) return;
        @NotNull final String userId = session.getId();
        @Nullable final List<Project> projectList = serviceLocator.getProjectEndpoint().findAllProjectsBySort(session);
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            Project project = projectList.get(i);
            System.out.println(i+1 + ". " + ProjectPrintUtil.print(project));
        }
    }

}
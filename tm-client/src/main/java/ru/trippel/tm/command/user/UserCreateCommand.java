package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "user create";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create a user, you must enter a username and password.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter login name.");
        @NotNull final String loginName = serviceLocator.getTerminalService().read();
        if (loginName == null || loginName.isEmpty()){
            System.out.println("Invalid login.");
            return;
        }
        if (serviceLocator.getUserEndpoint().checkLoginUser(loginName)) {
            System.out.println("This name already exists, try again.");
            return;
        }
        System.out.println("Enter password.");
        @NotNull final String password = serviceLocator.getTerminalService().read();
        if (password == null || password.isEmpty()){
            System.out.println("Invalid password.");
            return;
        }
        serviceLocator.getUserEndpoint().createUser(loginName, password);
        if (serviceLocator.getUserEndpoint().checkLoginUser(loginName))
        System.out.printf("Login \"%s\" added!\n", loginName);
    }

}

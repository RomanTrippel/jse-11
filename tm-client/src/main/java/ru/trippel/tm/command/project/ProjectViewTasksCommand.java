package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.*;
import ru.trippel.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
public final class ProjectViewTasksCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project view task";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View all attached tasks.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @Nullable final List<Task> taskList = serviceLocator.getTaskEndpoint()
                .findAllTasksBySort(session);
        if (taskList == null || taskList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        @Nullable final List<Project> projectList = serviceLocator.getProjectEndpoint()
                .findAllProjectsBySort(session);
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number");
        projectNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String projectId = projectList.get(projectNum).getId();
        for (@NotNull final Task task : taskList) {
            if (task.getProjectId().equals(projectId)) {
                System.out.println(task.getName() + " - " + task.getDescription());
            }
        }
    }

}

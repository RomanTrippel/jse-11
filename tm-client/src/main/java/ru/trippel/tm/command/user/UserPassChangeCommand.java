package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.api.endpoint.User;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.util.PasswordHashUtil;

@NoArgsConstructor
public final class UserPassChangeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "user pass change";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change Password.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final String userId = serviceLocator.getStateService().getSession().getUserId();
        @Nullable final User userTemp = serviceLocator.getUserEndpoint().findOneUser(session, userId);
        if (userTemp == null) {
            System.out.println("Editable user is not defined.");
            return;
        }
        System.out.println("Enter new Password.");
        @NotNull final String passwordNew = serviceLocator.getTerminalService().read();
        @NotNull final String hashPasswordNew = PasswordHashUtil.getHash(passwordNew);
        userTemp.setPassword(hashPasswordNew);
        serviceLocator.getUserEndpoint().updateUser(session, userTemp);
        serviceLocator.getSessionEndpoint().removeSession(session);
        serviceLocator.getStateService().setSession(null);
        System.out.println("Changes applied. Login required again.");
    }

}

package ru.trippel.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.IOException_Exception;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.api.endpoint.TypeRole;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class DataFasterxmlJsonLoadCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data fasterxml json load";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Loading data as Json using FasterXML.";
    }

    @Override
    public void execute() throws Exception_Exception, IOException_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        serviceLocator.getDataEndpoint().dataFasterxmlJsonLoad(session);
        serviceLocator.getSessionEndpoint().removeSession(session);
        serviceLocator.getStateService().setSession(null);
        System.out.println("Data loaded.");
        System.out.println("You need to log in again.");
    }

}

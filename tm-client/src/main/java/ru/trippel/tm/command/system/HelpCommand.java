package ru.trippel.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "help";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Commands list.";
    }

    @Override
    public void execute() {
        System.out.println("Commands list:");
        for (@NotNull final AbstractCommand command :
                serviceLocator.getStateService().getCommands()) {
            System.out.println(command.getNameCommand() + ": "
                    + command.getDescription());
        }
    }

}
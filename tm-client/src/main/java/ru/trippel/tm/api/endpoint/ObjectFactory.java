
package ru.trippel.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.trippel.tm.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ClassNotFoundException_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "ClassNotFoundException");
    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "Exception");
    private final static QName _IOException_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "IOException");
    private final static QName _JAXBException_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "JAXBException");
    private final static QName _DataFasterxmlJsonLoad_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataFasterxmlJsonLoad");
    private final static QName _DataFasterxmlJsonLoadResponse_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataFasterxmlJsonLoadResponse");
    private final static QName _DataFasterxmlJsonSave_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataFasterxmlJsonSave");
    private final static QName _DataFasterxmlJsonSaveResponse_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataFasterxmlJsonSaveResponse");
    private final static QName _DataFasterxmlXmlLoad_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataFasterxmlXmlLoad");
    private final static QName _DataFasterxmlXmlLoadResponse_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataFasterxmlXmlLoadResponse");
    private final static QName _DataFasterxmlXmlSave_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataFasterxmlXmlSave");
    private final static QName _DataFasterxmlXmlSaveResponse_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataFasterxmlXmlSaveResponse");
    private final static QName _DataJaxbJsonLoad_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataJaxbJsonLoad");
    private final static QName _DataJaxbJsonLoadResponse_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataJaxbJsonLoadResponse");
    private final static QName _DataJaxbJsonSave_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataJaxbJsonSave");
    private final static QName _DataJaxbJsonSaveResponse_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataJaxbJsonSaveResponse");
    private final static QName _DataJaxbXmlLoad_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataJaxbXmlLoad");
    private final static QName _DataJaxbXmlLoadResponse_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataJaxbXmlLoadResponse");
    private final static QName _DataJaxbXmlSave_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataJaxbXmlSave");
    private final static QName _DataJaxbXmlSaveResponse_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataJaxbXmlSaveResponse");
    private final static QName _DataSerializationLoad_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataSerializationLoad");
    private final static QName _DataSerializationLoadResponse_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataSerializationLoadResponse");
    private final static QName _DataSerializationSave_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataSerializationSave");
    private final static QName _DataSerializationSaveResponse_QNAME = new QName("http://endpoint.api.tm.trippel.ru/", "dataSerializationSaveResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.trippel.tm.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClassNotFoundException }
     * 
     */
    public ClassNotFoundException createClassNotFoundException() {
        return new ClassNotFoundException();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link IOException }
     * 
     */
    public IOException createIOException() {
        return new IOException();
    }

    /**
     * Create an instance of {@link JAXBException }
     * 
     */
    public JAXBException createJAXBException() {
        return new JAXBException();
    }

    /**
     * Create an instance of {@link DataFasterxmlJsonLoad }
     * 
     */
    public DataFasterxmlJsonLoad createDataFasterxmlJsonLoad() {
        return new DataFasterxmlJsonLoad();
    }

    /**
     * Create an instance of {@link DataFasterxmlJsonLoadResponse }
     * 
     */
    public DataFasterxmlJsonLoadResponse createDataFasterxmlJsonLoadResponse() {
        return new DataFasterxmlJsonLoadResponse();
    }

    /**
     * Create an instance of {@link DataFasterxmlJsonSave }
     * 
     */
    public DataFasterxmlJsonSave createDataFasterxmlJsonSave() {
        return new DataFasterxmlJsonSave();
    }

    /**
     * Create an instance of {@link DataFasterxmlJsonSaveResponse }
     * 
     */
    public DataFasterxmlJsonSaveResponse createDataFasterxmlJsonSaveResponse() {
        return new DataFasterxmlJsonSaveResponse();
    }

    /**
     * Create an instance of {@link DataFasterxmlXmlLoad }
     * 
     */
    public DataFasterxmlXmlLoad createDataFasterxmlXmlLoad() {
        return new DataFasterxmlXmlLoad();
    }

    /**
     * Create an instance of {@link DataFasterxmlXmlLoadResponse }
     * 
     */
    public DataFasterxmlXmlLoadResponse createDataFasterxmlXmlLoadResponse() {
        return new DataFasterxmlXmlLoadResponse();
    }

    /**
     * Create an instance of {@link DataFasterxmlXmlSave }
     * 
     */
    public DataFasterxmlXmlSave createDataFasterxmlXmlSave() {
        return new DataFasterxmlXmlSave();
    }

    /**
     * Create an instance of {@link DataFasterxmlXmlSaveResponse }
     * 
     */
    public DataFasterxmlXmlSaveResponse createDataFasterxmlXmlSaveResponse() {
        return new DataFasterxmlXmlSaveResponse();
    }

    /**
     * Create an instance of {@link DataJaxbJsonLoad }
     * 
     */
    public DataJaxbJsonLoad createDataJaxbJsonLoad() {
        return new DataJaxbJsonLoad();
    }

    /**
     * Create an instance of {@link DataJaxbJsonLoadResponse }
     * 
     */
    public DataJaxbJsonLoadResponse createDataJaxbJsonLoadResponse() {
        return new DataJaxbJsonLoadResponse();
    }

    /**
     * Create an instance of {@link DataJaxbJsonSave }
     * 
     */
    public DataJaxbJsonSave createDataJaxbJsonSave() {
        return new DataJaxbJsonSave();
    }

    /**
     * Create an instance of {@link DataJaxbJsonSaveResponse }
     * 
     */
    public DataJaxbJsonSaveResponse createDataJaxbJsonSaveResponse() {
        return new DataJaxbJsonSaveResponse();
    }

    /**
     * Create an instance of {@link DataJaxbXmlLoad }
     * 
     */
    public DataJaxbXmlLoad createDataJaxbXmlLoad() {
        return new DataJaxbXmlLoad();
    }

    /**
     * Create an instance of {@link DataJaxbXmlLoadResponse }
     * 
     */
    public DataJaxbXmlLoadResponse createDataJaxbXmlLoadResponse() {
        return new DataJaxbXmlLoadResponse();
    }

    /**
     * Create an instance of {@link DataJaxbXmlSave }
     * 
     */
    public DataJaxbXmlSave createDataJaxbXmlSave() {
        return new DataJaxbXmlSave();
    }

    /**
     * Create an instance of {@link DataJaxbXmlSaveResponse }
     * 
     */
    public DataJaxbXmlSaveResponse createDataJaxbXmlSaveResponse() {
        return new DataJaxbXmlSaveResponse();
    }

    /**
     * Create an instance of {@link DataSerializationLoad }
     * 
     */
    public DataSerializationLoad createDataSerializationLoad() {
        return new DataSerializationLoad();
    }

    /**
     * Create an instance of {@link DataSerializationLoadResponse }
     * 
     */
    public DataSerializationLoadResponse createDataSerializationLoadResponse() {
        return new DataSerializationLoadResponse();
    }

    /**
     * Create an instance of {@link DataSerializationSave }
     * 
     */
    public DataSerializationSave createDataSerializationSave() {
        return new DataSerializationSave();
    }

    /**
     * Create an instance of {@link DataSerializationSaveResponse }
     * 
     */
    public DataSerializationSaveResponse createDataSerializationSaveResponse() {
        return new DataSerializationSaveResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link Throwable }
     * 
     */
    public Throwable createThrowable() {
        return new Throwable();
    }

    /**
     * Create an instance of {@link StackTraceElement }
     * 
     */
    public StackTraceElement createStackTraceElement() {
        return new StackTraceElement();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClassNotFoundException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "ClassNotFoundException")
    public JAXBElement<ClassNotFoundException> createClassNotFoundException(ClassNotFoundException value) {
        return new JAXBElement<ClassNotFoundException>(_ClassNotFoundException_QNAME, ClassNotFoundException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "IOException")
    public JAXBElement<IOException> createIOException(IOException value) {
        return new JAXBElement<IOException>(_IOException_QNAME, IOException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JAXBException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "JAXBException")
    public JAXBElement<JAXBException> createJAXBException(JAXBException value) {
        return new JAXBElement<JAXBException>(_JAXBException_QNAME, JAXBException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataFasterxmlJsonLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataFasterxmlJsonLoad")
    public JAXBElement<DataFasterxmlJsonLoad> createDataFasterxmlJsonLoad(DataFasterxmlJsonLoad value) {
        return new JAXBElement<DataFasterxmlJsonLoad>(_DataFasterxmlJsonLoad_QNAME, DataFasterxmlJsonLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataFasterxmlJsonLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataFasterxmlJsonLoadResponse")
    public JAXBElement<DataFasterxmlJsonLoadResponse> createDataFasterxmlJsonLoadResponse(DataFasterxmlJsonLoadResponse value) {
        return new JAXBElement<DataFasterxmlJsonLoadResponse>(_DataFasterxmlJsonLoadResponse_QNAME, DataFasterxmlJsonLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataFasterxmlJsonSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataFasterxmlJsonSave")
    public JAXBElement<DataFasterxmlJsonSave> createDataFasterxmlJsonSave(DataFasterxmlJsonSave value) {
        return new JAXBElement<DataFasterxmlJsonSave>(_DataFasterxmlJsonSave_QNAME, DataFasterxmlJsonSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataFasterxmlJsonSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataFasterxmlJsonSaveResponse")
    public JAXBElement<DataFasterxmlJsonSaveResponse> createDataFasterxmlJsonSaveResponse(DataFasterxmlJsonSaveResponse value) {
        return new JAXBElement<DataFasterxmlJsonSaveResponse>(_DataFasterxmlJsonSaveResponse_QNAME, DataFasterxmlJsonSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataFasterxmlXmlLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataFasterxmlXmlLoad")
    public JAXBElement<DataFasterxmlXmlLoad> createDataFasterxmlXmlLoad(DataFasterxmlXmlLoad value) {
        return new JAXBElement<DataFasterxmlXmlLoad>(_DataFasterxmlXmlLoad_QNAME, DataFasterxmlXmlLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataFasterxmlXmlLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataFasterxmlXmlLoadResponse")
    public JAXBElement<DataFasterxmlXmlLoadResponse> createDataFasterxmlXmlLoadResponse(DataFasterxmlXmlLoadResponse value) {
        return new JAXBElement<DataFasterxmlXmlLoadResponse>(_DataFasterxmlXmlLoadResponse_QNAME, DataFasterxmlXmlLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataFasterxmlXmlSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataFasterxmlXmlSave")
    public JAXBElement<DataFasterxmlXmlSave> createDataFasterxmlXmlSave(DataFasterxmlXmlSave value) {
        return new JAXBElement<DataFasterxmlXmlSave>(_DataFasterxmlXmlSave_QNAME, DataFasterxmlXmlSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataFasterxmlXmlSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataFasterxmlXmlSaveResponse")
    public JAXBElement<DataFasterxmlXmlSaveResponse> createDataFasterxmlXmlSaveResponse(DataFasterxmlXmlSaveResponse value) {
        return new JAXBElement<DataFasterxmlXmlSaveResponse>(_DataFasterxmlXmlSaveResponse_QNAME, DataFasterxmlXmlSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJaxbJsonLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataJaxbJsonLoad")
    public JAXBElement<DataJaxbJsonLoad> createDataJaxbJsonLoad(DataJaxbJsonLoad value) {
        return new JAXBElement<DataJaxbJsonLoad>(_DataJaxbJsonLoad_QNAME, DataJaxbJsonLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJaxbJsonLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataJaxbJsonLoadResponse")
    public JAXBElement<DataJaxbJsonLoadResponse> createDataJaxbJsonLoadResponse(DataJaxbJsonLoadResponse value) {
        return new JAXBElement<DataJaxbJsonLoadResponse>(_DataJaxbJsonLoadResponse_QNAME, DataJaxbJsonLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJaxbJsonSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataJaxbJsonSave")
    public JAXBElement<DataJaxbJsonSave> createDataJaxbJsonSave(DataJaxbJsonSave value) {
        return new JAXBElement<DataJaxbJsonSave>(_DataJaxbJsonSave_QNAME, DataJaxbJsonSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJaxbJsonSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataJaxbJsonSaveResponse")
    public JAXBElement<DataJaxbJsonSaveResponse> createDataJaxbJsonSaveResponse(DataJaxbJsonSaveResponse value) {
        return new JAXBElement<DataJaxbJsonSaveResponse>(_DataJaxbJsonSaveResponse_QNAME, DataJaxbJsonSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJaxbXmlLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataJaxbXmlLoad")
    public JAXBElement<DataJaxbXmlLoad> createDataJaxbXmlLoad(DataJaxbXmlLoad value) {
        return new JAXBElement<DataJaxbXmlLoad>(_DataJaxbXmlLoad_QNAME, DataJaxbXmlLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJaxbXmlLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataJaxbXmlLoadResponse")
    public JAXBElement<DataJaxbXmlLoadResponse> createDataJaxbXmlLoadResponse(DataJaxbXmlLoadResponse value) {
        return new JAXBElement<DataJaxbXmlLoadResponse>(_DataJaxbXmlLoadResponse_QNAME, DataJaxbXmlLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJaxbXmlSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataJaxbXmlSave")
    public JAXBElement<DataJaxbXmlSave> createDataJaxbXmlSave(DataJaxbXmlSave value) {
        return new JAXBElement<DataJaxbXmlSave>(_DataJaxbXmlSave_QNAME, DataJaxbXmlSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJaxbXmlSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataJaxbXmlSaveResponse")
    public JAXBElement<DataJaxbXmlSaveResponse> createDataJaxbXmlSaveResponse(DataJaxbXmlSaveResponse value) {
        return new JAXBElement<DataJaxbXmlSaveResponse>(_DataJaxbXmlSaveResponse_QNAME, DataJaxbXmlSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSerializationLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataSerializationLoad")
    public JAXBElement<DataSerializationLoad> createDataSerializationLoad(DataSerializationLoad value) {
        return new JAXBElement<DataSerializationLoad>(_DataSerializationLoad_QNAME, DataSerializationLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSerializationLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataSerializationLoadResponse")
    public JAXBElement<DataSerializationLoadResponse> createDataSerializationLoadResponse(DataSerializationLoadResponse value) {
        return new JAXBElement<DataSerializationLoadResponse>(_DataSerializationLoadResponse_QNAME, DataSerializationLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSerializationSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataSerializationSave")
    public JAXBElement<DataSerializationSave> createDataSerializationSave(DataSerializationSave value) {
        return new JAXBElement<DataSerializationSave>(_DataSerializationSave_QNAME, DataSerializationSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSerializationSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.trippel.ru/", name = "dataSerializationSaveResponse")
    public JAXBElement<DataSerializationSaveResponse> createDataSerializationSaveResponse(DataSerializationSaveResponse value) {
        return new JAXBElement<DataSerializationSaveResponse>(_DataSerializationSaveResponse_QNAME, DataSerializationSaveResponse.class, null, value);
    }

}

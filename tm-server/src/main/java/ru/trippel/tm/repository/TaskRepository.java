package ru.trippel.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.entity.Task;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> listTemp = new LinkedList<>();
        @NotNull final List<Task> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Task task: listAll) {
            if (task.getUserId().equals(userId)) listTemp.add(task);
        }
        return listTemp;
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator<Task> comparator) {
        @NotNull final List<Task> listResult = new LinkedList<>();
        @NotNull final List<Task> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Task task: listAll) {
            if (task.getUserId().equals(userId)) listResult.add(task);
        }
        listResult.sort(comparator);
        return listResult;
    }

    @NotNull
    @Override
    public List<Task> findByPart(@NotNull String userId, @NotNull String searchText) {
        @NotNull final List<Task> listResult = new LinkedList<>();
        @NotNull final List<Task> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Task task: listAll) {
            boolean foundInName = task.getName().toLowerCase().contains(searchText.toLowerCase());
            boolean foundInDescription = task.getDescription().toLowerCase().contains(searchText.toLowerCase());
            if (foundInName || foundInDescription) {
                listResult.add(task);
            }
        }
        return listResult;
    }

    @Override
    public void clear(@NotNull final String userId){
        @NotNull final List<Task> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Task task: listAll) {
            if (task.getUserId().equals(userId)) map.remove(task.getId());
        }
    }

    @Override
    public void removeByProjectId(@NotNull final String projectId) {
        @NotNull final List<Task> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Task task: listAll) {
            if (task.getProjectId().equals(projectId)) map.remove(task.getId());
        }
    }

    @Override
    public void persist(@NotNull List<Task> taskList) {
        map.clear();
        for (@NotNull final Task task: taskList) {
            if (map.containsKey(task.getId())) continue;
            map.put(task.getId(),task);
        }
    }

}
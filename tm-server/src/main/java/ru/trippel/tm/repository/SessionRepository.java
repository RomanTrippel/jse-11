package ru.trippel.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.ISessionRepository;
import ru.trippel.tm.entity.Session;

@NoArgsConstructor
public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Nullable
    @Override
    public Session findOne(@NotNull String sessionId, @NotNull String userId) {
        return null;
    }

    @Nullable
    @Override
    public Session persist(@NotNull String sessionId, @NotNull String userId) {
        return null;
    }

    @Nullable
    @Override
    public Session remove(@NotNull String sessionId, @NotNull String userId) {
        return null;
    }

}

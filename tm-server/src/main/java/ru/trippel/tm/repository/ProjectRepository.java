package ru.trippel.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.entity.Project;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> listResult = new LinkedList<>();
        @NotNull final List<Project> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Project project: listAll) {
            if (project.getUserId().equals(userId)) listResult.add(project);
        }
        return listResult;
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final Comparator<Project> comparator) {
        @NotNull final List<Project> listResult = new LinkedList<>();
        @NotNull final List<Project> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Project project: listAll) {
            if (project.getUserId().equals(userId)) listResult.add(project);
        }
        listResult.sort(comparator);
        return listResult;
    }

    @NotNull
    @Override
    public List<Project> findByPart(@NotNull String userId, @NotNull String searchText) {
        @NotNull final List<Project> listResult = new LinkedList<>();
        @NotNull final List<Project> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Project project: listAll) {
            boolean foundInName = project.getName().toLowerCase().contains(searchText.toLowerCase());
            boolean foundInDescription = project.getDescription().toLowerCase().contains(searchText.toLowerCase());
            if (foundInName || foundInDescription) {
                listResult.add(project);
            }
        }
        return listResult;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Project> listAll  = new LinkedList<>(map.values());
        for (@NotNull final Project project: listAll) {
            if (project.getUserId().equals(userId)) map.remove(project.getId());
        }
    }

    @Override
    public void persist(@NotNull final List<Project> projectList) {
        map.clear();
        for (@NotNull final Project project: projectList) {
            if (map.containsKey(project.getId())) continue;
            map.put(project.getId(),project);
        }
    }

}

package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class PasswordHashUtil {

    @Nullable
    public static String getHash(@NotNull final String pass) {
        @NotNull final String salt = "~какаятосоль*";
        @NotNull final String passWithSalt = String.format("%s %s %s", salt, pass, salt);
        try {
            @NotNull final MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(passWithSalt.getBytes());
            @NotNull final BigInteger bigInt = new BigInteger(1, digest.digest());
            @NotNull final String result = bigInt.toString(16);
            return result;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.entity.ISortable;
import ru.trippel.tm.enumeration.SortingMethod;
import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorUtil {

    @NotNull
    public static <T extends ISortable> Comparator <T> getComparator(@NotNull SortingMethod sortingMethod){
        switch (sortingMethod) {
            case CREATION_ORDER:
                break;
            case START_DATE:
                return new Comparator<T>() {
                    @Override
                    public int compare(@NotNull final T o1,@NotNull final T o2) {
                        return  (o1.getDateStart().compareTo(o2.getDateStart()));
                    }
                };
            case FINISH_DATE:
                return new Comparator<T>() {
                    @Override
                    public int compare(@NotNull final T o1,@NotNull final T o2) {
                        return  (o1.getDateFinish().compareTo(o2.getDateFinish()));
                    }
                };
            case STATUS:
                return new Comparator<T>() {
                    @Override
                    public int compare(@NotNull final T o1,@NotNull final T o2) {
                        return  (o1.getStatus().compareTo(o2.getStatus()));
                    }
                };
        }

        return new Comparator<T>() {
            @Override
            public int compare(@NotNull final T o1,@NotNull final T o2) {
                return 0;
            }
        };
    }

}

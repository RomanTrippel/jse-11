package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.ISessionRepository;
import ru.trippel.tm.api.service.ISessionService;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.enumeration.TypeRole;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    public SessionService(@NotNull final ISessionRepository repository) {
        super(repository);
        sessionRepository = repository;
    }

    @Nullable
    @Override
    public Session findOne(@Nullable String sessionId, @Nullable String userId) {
        if (sessionId == null || sessionId.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return sessionRepository.findOne(sessionId,userId);
    }

    @Nullable
    @Override
    public Session persist(@Nullable String sessionId, @Nullable String userId) {
        if (sessionId == null || sessionId.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return sessionRepository.persist(sessionId,userId);
    }

    @Nullable
    @Override
    public Session remove(@Nullable String sessionId, @Nullable String userId) {
        if (sessionId == null || sessionId.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return sessionRepository.remove(sessionId,userId);
    }

    @Override
    public void validate(@Nullable Session session) throws Exception {
        if (session == null) throw new Exception("You must log in.");
        if (session.getUserId() == null ||
                session.getSignature() == null ||
                session.getRole() == null) throw new Exception("You must log in.");
        @Nullable final Session sessionInBase = sessionRepository.findOne(session.getId());
        if (sessionInBase == null) throw new Exception("No session.");
        @Nullable final String signature = session.getSignature();
        @Nullable final String signatureInBase = sessionInBase.getSignature();
        if (signatureInBase == null) throw new Exception("You must log in.");
        if (!(signatureInBase.equals(signature))) throw new Exception("You must log in.");
        final long passedTime = System.currentTimeMillis() - session.getCreateDate().getTime();
        if (passedTime > 10*60*1000) {
            repository.remove(session.getId());
            throw new Exception("The session time of 10 minutes has expired. You need to log in.");
        }
    }

    @Override
    public void validate(@Nullable Session session, @NotNull TypeRole role) throws Exception {
        if (!role.equals(session.getRole())) throw new Exception("Not enough rights.");
        validate(session);
    }

}

package ru.trippel.tm.api.service;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface IDataService {

    void dataSerializationSave() throws IOException;

    void dataFasterxmlXmlSave() throws IOException;

    void dataFasterxmlJsonSave() throws IOException;

    void dataJaxbXmlSave() throws JAXBException;

    void dataJaxbJsonSave() throws JAXBException;

    void dataSerializationLoad() throws IOException, ClassNotFoundException;

    void dataFasterxmlXmlLoad() throws IOException;

    void dataFasterxmlJsonLoad() throws IOException;

    void dataJaxbXmlLoad() throws JAXBException;

    void dataJaxbJsonLoad() throws JAXBException;

}

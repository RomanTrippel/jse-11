package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.enumeration.TypeRole;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session findOne(@Nullable String sessionId, @Nullable String userId);

    @Nullable
    Session persist (@Nullable String sessionId, @Nullable String userId);

    @Nullable
    Session remove (@Nullable String sessionId, @Nullable String userId);

    void validate (@Nullable Session session) throws Exception;

    void validate (@Nullable Session session, @NotNull TypeRole role) throws Exception;

}

package ru.trippel.tm.enumeration;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum Status {

    @NotNull
    PLANNED("Planned"),
    @NotNull
    IN_PROGRESS("In progress"),
    @NotNull
    READY("Ready");

    @Getter
    @NotNull
    private String displayName = "";

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
